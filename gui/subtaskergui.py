from easygui import *

#
# Jira login
#
def jiraLoginGui():
	msg = "Enter login information"
	title = "Subtask Generator"
	fieldNames = ['Jira User ID', 'Password']
	fieldValues = []
	fieldValues = multpasswordbox(msg, title, fieldNames)

	# make sure that none of the fields was left blank
	while 1:
		if fieldValues == None: break
		errmsg = ""
		for i in range(len(fieldNames)):
			if fieldValues[i].strip() == "":
				errmsg = errmsg + ('"%s" is a required field.\n\n' % fieldNames[i])
		if errmsg == "": break  # no problems found
		fieldValues = multpasswordbox(errmsg, title, fieldNames, fieldValues)

	assert isinstance(fieldValues, object)
	if not fieldValues:
		return dict()
	return (dict([('username', fieldValues[0]) , ('password', fieldValues[1])]))

def jiraTicketGui():
	while True:
		#
		# Issue data
		#
		jira_ticket = enterbox("Enter Ticket # w/out the SEOM- or nothing to exit", "Select Ticket")
		if jira_ticket == None: return None

		if len(jira_ticket) != 0:
			jira_ticket = 'SEOM-' + jira_ticket

		print jira_ticket
		return jira_ticket

def jiraVersionGui():
		msg = "Target Build Version"
		fieldNames = ['7.0 Branch', '7.5 Branch', '8.0 Branch']
		fieldValues = ['Next_70_Maintenance_Release', 'Next_75_Maintenance_Release', 'Next_80_Maintenance_Release']
		fieldValues = multenterbox(msg, "", fieldNames, fieldValues)
		print fieldValues
		versions = {}

		if fieldValues == None: return None

		if fieldValues[0].strip() != "":
			versions['70'] = fieldValues[0].strip()

		if fieldValues[1].strip() != "":
			versions['75'] = fieldValues[1].strip()

		if fieldValues[2].strip() != "":
			versions['80'] = fieldValues[2].strip()

		return versions

