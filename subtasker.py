from jira.client import JIRA
import os, sys, inspect
from Tkinter import *
from gui.subtaskergui import *
from dumpObj import *

cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0]) + "/gui")
if cmd_folder not in sys.path:
	sys.path.insert(0, cmd_folder)

# jira_user = 'some'
# jira_password = 'one'
jira_server = 'https://jira.monsooncommerce.com/'
jira_project_key = 'SEOM'

options = {
	'server': jira_server
}

# ----------------------------------------
# Login into Jira
# ----------------------------------------
login_info = jiraLoginGui()

if not login_info:
	exit(0)

jira_user = login_info["username"]
jira_password = login_info["password"]

# print "Login " + jira_user + "  " + jira_password

jira = JIRA(options, basic_auth=(jira_user, jira_password))
print "auth success"



# --------------------------------------
#   Issue and update loop
# --------------------------------------

while True:
	# seom_ticket = raw_input("Enter a Jira ticket e.g. SEOM # w/out the SEOM-, or nothing to exit > ")

	seom_ticket = jiraTicketGui()

	if seom_ticket == None or len(seom_ticket) == 0:
		exit(0)
	# else:
	# 	seom_ticket = "SEOM-" + seom_ticket

	parent = jira.issue(seom_ticket)
	parent_summary = parent.fields.summary
	parent_description = parent.fields.description
	parent_priority = parent.fields.priority

	#dumpObj(parent_priority)
	print ("Priority is " + parent_priority.name)
	subtask_summary = seom_ticket + " - " + parent_summary

	# target_versions = raw_input("Enter the desired versions separated by space (70 75 80) > ")
	target_versions = jiraVersionGui()

	if target_versions == None: continue

	if '70' in target_versions:
		subtask_70 = {
			'project': {'key': 'SEOM'},
			'summary': subtask_summary + " => Fix for 7.0",
			'description': parent_description,
			'priority': {'id':parent_priority.id},
			'issuetype': {'name': 'Sub-task'},
			'customfield_11603': [{"name": target_versions['70']}],
			'parent': {'id': seom_ticket},
		}

		child = jira.create_issue(fields=subtask_70)
		print("created child: " + child.key)

	if '75' in target_versions:
		subtask_75 = {
			'project': {'key': 'SEOM'},
			'summary': subtask_summary + " => Fix for 7.5",
			'description': parent_description,
			'priority': {'id':parent_priority.id},
			'issuetype': {'name': 'Sub-task'},
			'customfield_11603': [{"name": target_versions['75']}],
			'parent': {'id': seom_ticket},
		}

		child = jira.create_issue(fields=subtask_75)
		print("created child: " + child.key)

	if '80' in target_versions:
		subtask_80 = {
			'project': {'key': 'SEOM'},
			'summary': subtask_summary + " => Fix for 8.0",
			'description': parent_description,
			'priority': {'id':parent_priority.id},
			'issuetype': {'name': 'Sub-task'},
			'customfield_11603': [{"name": target_versions['80']}],
			'parent': {'id': seom_ticket},
		}

		child = jira.create_issue(fields=subtask_80)
		print("created child: " + child.key)
